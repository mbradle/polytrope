import polytrope_plot as pp

#
#  Get string for title
#

f = open( 'data.txt', 'r' )
my_title = f.readline()
f.close()

#
#  Create plot--change data.txt, x, y, and labels, as desired
#

pp.polytrope_plot(\
  'data.txt', x = 'xi', y = 'theta', xlabel = '$\\xi$',\
  ylabel = '$\\theta(\\xi)$', title = my_title\
)
