/*
  Compile:  g++ `gsl-config --cflags --libs` -o polytrope polytrope.cpp

  Run (for example): ./polytrope 3 1.e30 1.e7 > data.txt
*/

////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code for computing a polytrope.
////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

#include <boost/numeric/odeint.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>

#include <gsl/gsl_math.h>
#include <gsl/gsl_const_cgsm.h>

typedef std::vector< double > state_type;

#define  S_XI        "xi"
#define  S_THETA     "theta"
#define  S_DTHETA    "dtheta"
#define  S_RHO_C     "rho_c"
#define  S_P_C       "P_c"
#define  S_MASS      "mass"
#define  S_RADIUS    "radius"
#define  S_DENSITY   "density"
#define  S_PRESSURE  "pressure"

class polytrope_rhs
{

  public:
    polytrope_rhs( double n ) : m_n( n ) { }

    void operator() ( const state_type &x , state_type &dxdt , const double t )
    {

      if( t > 0 )
      {
        dxdt[0] = x[1];
        if( x[0] < 0 )
        {
          if( fabs( x[0] ) > 1.e-4 )
          {
            std::cerr << t << "  " << x[0] << std::endl;
            std::cerr << "Theta large and negative in integration\n";
            exit( EXIT_FAILURE );
          }
          dxdt[1] = - 2. * x[1] / t;
        }
        else
        {
          dxdt[1] = -pow( x[0], m_n ) - 2. * x[1] / t;
        }
      }
      else
      {
        dxdt[0] = 0.;
        dxdt[1] = -1. / 3.;
      }
    }

  private:
    double m_n;

};

class polytrope
{

  public:
    polytrope( double _n ) : n( _n )
    {
      std::string n_up = "4.9999999";
      if( n < 0 || n > boost::lexical_cast<double>( n_up ) )
      {
        std::cerr << "Polytropic index restricted to 0 <= n <= " <<
                     n_up << std::endl;
        exit( EXIT_FAILURE );
      }
        
      initialize();
    }

    void integrate_range( state_type& x, const double t1, const double t2 )
    { 

      using namespace boost::numeric::odeint;
      polytrope_rhs my_rhs( n );
      double dt = 0.01;

      integrate( my_rhs, x, t1, t2, dt );

    }

    std::pair<double, double>
    computeThetaAndDerivative( const double xi )
    {

      if( xi == 0 )
      {
        return std::make_pair( 1., 0. );
      }
      else if( fabs( ( xi - d_xi1 ) / d_xi1 ) < 1.e-8 )
      {
        return std::make_pair( 0., d_theta_dxi1 );
      }

      state_type x(2);
      x[0] = 1.0;
      x[1] = 0.0;
      size_t i_div = 10;

      double t = 0;
      double dt = xi / i_div;

      for( size_t i = 0; i < i_div; i++ )
      {
        integrate_range( x, t, t + dt );
        t += dt;
      }

      return std::make_pair( x[0], x[1] );

    }

    void
    initialize()
    {

      state_type x(2);
      x[0] = 1.0;
      x[1] = 0.0;
      double t = 0;
      double dt = 1.e-4;

      while( x[0] > 1.e-20 )
      {

        state_type xold;
        std::copy( x.begin(), x.end(), std::back_inserter( xold ) );
        integrate_range( x, t, t + dt );
        t += dt;
        double dtp = 1.15 * dt;
        for( size_t i = 0; i < xold.size(); i++ )
        {
          double d = ( x[i] - xold[i] ) / dt;
          double dtm = 0.15 * fabs( x[i] / (d + 1.e-300) );
          if( dtm < dtp ) { dtp = dtm; }
        }
        dt = dtp;

      }

      d_xi1 = t;
      d_theta_dxi1 = x[1];
      
    }

    double getN() { return n; }

    double getXi1() { return d_xi1; }

    double getdThetadXi1() { return d_theta_dxi1; }

  private:
    double n, d_xi1, d_theta_dxi1;

};

class physical_polytrope : public polytrope
{

  public:
    physical_polytrope( double _n ) : polytrope ( _n ) {
      r.reset(); 
      Pc.reset();
      rhoc.reset();
      m.reset();
      p_map[S_RADIUS] = r;
      p_map[S_P_C] = Pc;
      p_map[S_RHO_C] = rhoc;
      p_map[S_MASS] = m;
    }

    void checkQuantity( std::string s )
    {
      if( p_map.find( s ) == p_map.end() )
      {
        std::cerr << "No such quantity " << s << "!" << std::endl;
        exit( EXIT_FAILURE );
      }
    }

    void
    clearQuantity( std::string s )
    {
      checkQuantity( s );
      p_map[s].reset();
    }

    void
    setQuantity( std::string s, double x )
    {
      checkQuantity( s );
      if( !p_map[s] )
      {
        p_map[s].reset( new double );
      }
      *(p_map[s]) = x;
    }

    double
    getQuantity( std::string s )
    {
      checkQuantity( s );
      if( !p_map[s] )
      {
        std::cerr << "Quantity " << s << " not set!" << std::endl;
      }
      return *(p_map[s]);
    }

    bool
    hasQuantity( std::string s )
    {
      checkQuantity( s );
      if( p_map[s] )
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    double
    getRadius()
    {

      if( hasQuantity( S_RADIUS ) ) { return getQuantity( S_RADIUS ); }

      if( hasQuantity( S_P_C ) && hasQuantity( S_RHO_C ) )
      {
        return
          sqrt(
            ( getN() + 1 ) * getQuantity( S_P_C ) /
            (
              4. * M_PI *
                   GSL_CONST_CGSM_GRAVITATIONAL_CONSTANT *
                   gsl_pow_2( getQuantity( S_RHO_C ) )
            )
          );
      }
    }

    double
    getRadius( double xi )
    {
      return getRadius() * xi;
    }

    double
    getMass( double xi )
    {
      if( hasQuantity( S_P_C ) && hasQuantity( S_RHO_C ) )
      {
        double r = getRadius();
        std::pair<double, double> p = computeThetaAndDerivative( xi );

        return
          -4. * M_PI * gsl_pow_3( r ) * gsl_pow_2( xi ) * p.second;
      }
    }

    double
    getDensity( double xi )
    {
      if( hasQuantity( S_RHO_C ) )
      {
        std::pair<double, double> p = computeThetaAndDerivative( xi );

        return *p_map[S_RHO_C] * pow( p.first, getN() );
      }
    }

    double
    getPressure( double xi )
    {
      if( hasQuantity( S_P_C ) )
      {
        std::pair<double, double> p = computeThetaAndDerivative( xi );

        return *p_map[S_P_C] * pow( p.first, getN() + 1 );
      }
    }

    double
    getMass()
    {
      if( hasQuantity( S_MASS ) ) { return *p_map[S_MASS]; }

      if( hasQuantity( S_P_C ) && hasQuantity( S_RHO_C ) )
      {
        double r = getRadius();

        return
          -4. * M_PI * gsl_pow_3( r ) *
                gsl_pow_2( getXi1() ) * getdThetadXi1();
      }
    }

    double
    computeCentralDensityToAverageDensity()
    {
      return -getXi1() / ( 3. * getdThetadXi1() );
    }

  private:
    boost::shared_ptr<double> r, Pc, rhoc, m;
    std::map<std::string, boost::shared_ptr<double> > p_map;

};
  

int main(int argc, char** argv )
{

    if( argc != 4 )
    {
      std::cerr << "\nUsage: n rho_c P_c\n";
      std::cerr << "   n = polytropic index\n\n"; 
      std::cerr << "   rho_c = central density (g/cc)\n\n"; 
      std::cerr << "   P_c = central pressure (dynes/cm^2)\n\n"; 
      exit( EXIT_FAILURE );
    }

    physical_polytrope my_polytrope( boost::lexical_cast<double>( argv[1] ) );

    std::cout << "n = " << my_polytrope.getN() << std::endl;

    std::cout << "xi1 = " << my_polytrope.getXi1() << "  " <<
                 "dxi1 = " <<
                 my_polytrope.getdThetadXi1() << std::endl;

    std::cout << "rho_c / <rho> = " << 
      my_polytrope.computeCentralDensityToAverageDensity() << std::endl;

    my_polytrope.setQuantity( S_RHO_C, boost::lexical_cast<double>( argv[2] ) );

    my_polytrope.setQuantity( S_P_C, boost::lexical_cast<double>( argv[3] ) );

    std::cout << S_XI << "  " <<
                 S_THETA << "  " <<
                 S_DTHETA << "  " <<
                 S_RADIUS << "  " <<
                 S_DENSITY << "  " <<
                 S_PRESSURE << "  " <<
                 S_MASS << std::endl;

    for( size_t i = 0; i <= 100; i++ )
    {
      double xi = (double) i * my_polytrope.getXi1() / 100.;
      std::pair<double, double> p2 =
        my_polytrope.computeThetaAndDerivative( xi );
      std::cout << xi << "  " << p2.first << "  " << p2.second << "  " << 
        my_polytrope.getRadius( xi ) << "  " <<
        my_polytrope.getDensity( xi ) << "  " <<
        my_polytrope.getPressure( xi ) << "  " <<
        my_polytrope.getMass( xi ) / GSL_CONST_CGSM_SOLAR_MASS << std::endl;
    }

    return 0;

}
