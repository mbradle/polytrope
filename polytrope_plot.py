import plot_params as plp
import matplotlib.pyplot as plt
import numpy as np

def set_hash( file ):
  f = open( file, 'r' )
  f.readline()
  f.readline()
  f.readline()
  line = f.readline()
  s = {}
  f.close()

  arr = line.split()
  i = 0
  for str in arr:
    s[str] = i
    i += 1
  return s

def polytrope_plot( file, **kwargs ):
  f = open( file, 'r' )
  data = np.genfromtxt( f, skip_header = 3 )
  f.close()

  s = set_hash( file )

  plp.set_plot_params( plt, kwargs )

  if( not ('x' in kwargs) ):
    print( "Must include x keyword." )
    exit(1)

  if( not ('y' in kwargs) ):
    print( "Must include y keyword." )
    exit(1)

  plt.plot( data[:,s[kwargs['x']]], data[:,s[kwargs['y']]] )

  plp.apply_class_methods( plt, kwargs )

  plt.show()
